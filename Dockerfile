FROM python:3-slim-buster AS build

RUN pip install poetry 
RUN apt update -y && apt install -y libpq-dev gcc make

WORKDIR /my_tooler

COPY poetry.lock .
COPY pyproject.toml .

RUN poetry config virtualenvs.create false
RUN poetry install 

COPY . .

RUN make all

ENTRYPOINT [ "python", "my_tooler/uvicorn_asgi.py " ]


