PROJECT_PATH = my_tooler

all: make_migrations apply_migrations

.PHONY: all



# make migration files
make_migrations:
	@echo "Make migrations...";
	python $(PROJECT_PATH)/manage.py makemigrations


# aplly migration on db 
apply_migrations: make_migrations	
	@echo "Apply migrations...";
	python $(PROJECT_PATH)/manage.py migrate
