from django.urls import path

from . import views

urlpatterns = [
    path('ajax/', views.ajax_consume, name='ajax_consume'),
]