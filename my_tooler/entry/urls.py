from django.urls import path

from . import views


urlpatterns = [
    path('', views.entry_login, name='login'),
]