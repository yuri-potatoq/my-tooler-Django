from django.http.response import HttpResponse
from django.template import loader

from django.conf import settings

def entry_login(request):
    template = loader.get_template('base.html')
    # template = loader.get_template('entry/login.html')
    
    return HttpResponse(template.render({}, request))
