from django.contrib import admin
from django.urls import path, include

from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),

    # dashboard and entrypoints to anothers apps
    path('entry/', include('entry.urls')),

    # several traits of ajax requests
    path('tools/', include('ajax_app.urls')),
    
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
